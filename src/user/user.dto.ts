export interface createUserDto {
  username: string;
  nama: string;
  email: string;
  password: string;
}

export interface updateUserDto {
  username: string;
  nama: string;
  email: string;
  password: string;
}

export interface getUserDto {
  id: string;
  password: string;
}
