import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { User } from 'src/schemas/user.scema';
import { createUserDto, getUserDto, updateUserDto } from './user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  async findAll(): Promise<User[]> {
    return this.userService.findAll();
  }

  @Post('register')
  register(@Body() cretaUser: createUserDto): Promise<User> {
    return this.userService.register(cretaUser);
  }

  @Post('login')
  login(
    @Body() getUser: getUserDto,
  ): Promise<{ message: string; user: User } | { message: string }> {
    return this.userService.login(getUser);
  }

  @Get(':id')
  getOne(@Param('id') id: string): Promise<User> {
    return this.userService.getOne(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateUserDto: updateUserDto) {
    return this.userService.update(updateUserDto, id);
  }

  @Delete(':id')
  delete(@Param('id') id: string): string {
    return this.userService.delete(id);
  }
}
