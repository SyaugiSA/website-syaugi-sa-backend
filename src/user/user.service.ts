import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from 'src/schemas/user.scema';
import { createUserDto, getUserDto, updateUserDto } from './user.dto';
import { createHmac } from 'crypto';

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private UserModel: Model<UserDocument>) {}

  async register(createUserDto: createUserDto): Promise<User> {
    createUserDto.password = createHmac(
      'sha256',
      createUserDto.password,
    ).digest('hex');
    const createUser = new this.UserModel(createUserDto);
    return createUser.save();
  }

  async findAll(): Promise<User[]> {
    return this.UserModel.find().exec();
  }

  async login(
    getUserDto: getUserDto,
  ): Promise<{ message: string; user: User } | { message: string }> {
    const newpassword = createHmac('sha256', getUserDto.password).digest('hex');

    if (
      await this.UserModel.findOne({
        email: getUserDto.id,
        password: newpassword,
      })
    ) {
      return {
        message: 'halo',
        user: await this.UserModel.findOne({
          email: getUserDto.id,
        }),
      };
    } else if (
      await this.UserModel.findOne({
        username: getUserDto.id,
        password: newpassword,
      })
    ) {
      return {
        message: 'halo',
        user: await this.UserModel.findOne({
          username: getUserDto.id,
        }),
      };
    } else {
      return { message: 'user tidak ada' };
    }
  }

  async getOne(id): Promise<User> {
    return await this.UserModel.findOne({ username: id });
  }

  update(updateUser: updateUserDto, id: string): string {
    if (updateUser.email) {
      this.UserModel.updateOne(
        { username: id },
        { email: updateUser.email },
        (err) => {
          if (err) return err;
        },
      );
    }

    if (updateUser.nama) {
      this.UserModel.updateOne(
        { username: id },
        { nama: updateUser.nama },
        (err) => {
          if (err) return err;
        },
      );
    }

    if (updateUser.password) {
      updateUser.password = createHmac('sha256', updateUser.password).digest(
        'hex',
      );
      this.UserModel.updateOne(
        { username: id },
        { password: updateUser.password },
        (err) => {
          if (err) return err;
        },
      );
    }

    if (updateUser.username) {
      this.UserModel.updateOne(
        { username: id },
        { username: updateUser.username },
        (err) => {
          if (err) return err;
        },
      );
    }
    return `data berhasil diganti`;
  }

  delete(id): string {
    this.UserModel.deleteOne({ username: id }, (err) => {
      if (err) return err;
    });
    return `user berhasil dihapus`;
  }
}
